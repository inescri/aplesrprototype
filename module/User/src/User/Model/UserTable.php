<?php

namespace User\Model;


use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class UserTable
{
    protected $table_gateway;

    public function __construct(TableGateway $table_gateway)
    {
        $this->table_gateway = $table_gateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->table_gateway->select();
        return $resultSet;
    }

    public function getUser($id)
    {
        $id  = (int) $id;
        $rowset = $this->table_gateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function fetchUserList()
    {
        $result_set = $this->table_gateway->select(function (Select $select)  {
            $select->columns(array(
                'id',
                'firstname',
                'lastname',
                'email',
            ));
            $select->join('user_detail',"user_id = id",array('detail_name', 'detail_value'),'left');
            
           // $select->join('user_detail','user.id = assignment.contact_id');
        } );

        return $result_set;
    }

    public function fetchUsersbyRole($role)
    {
        $result_set = $this->table_gateway->select(array('role' => $role));
        if (empty($result_set)) {
            return false;
        }
        return $result_set;
    }

    public function fetchUsersby($condition)
    {
        return 0;
    }

    public function getUserbyName($firstname, $lastname)
    {
        $rowset = $this->table_gateway->select(array('firstname' => $firstname ,'lastname' => $lastname));
         $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function saveUser(User $user)
    {
    	$data = (array) $user;
        $id = (int) $contact->id;
        if ($id == 0){
            $this->table_gateway->insert($data);
        }
        else {
            if ($this->getAlbum($id)) {
                $this->table_gateway->update($data, array('id' => $id));
            }
            else {
                throw new \Exception("Error Processing Request", 1);    
            }
        }
    }

 }