<?php
namespace User\Model;

class User
{
	public $id;
	public $username;
	public $password;
	public $firstname;
	public $lastname;
	public $email;
	public $status;
	public $role;
	public $country;
	
	public function exchangeArray($data) 
	{
		$this->id     = (isset($data['id'])) ? $data['id'] : null;
		$this->username = (isset($data['username'])) ? $data['username'] : null;
		$this->password = (isset($data['password'])) ? $data['password'] : null;
		$this->firstname = (isset($data['firstname'])) ? $data['firstname'] : null;
		$this->lastname = (isset($data['lastname']))? $data['lastname'] : null;
		$this->email = (isset($data['email']))? $data['email'] : null;
		$this->status = (isset($data['status']))? $data['status'] : null;
		$this->role = (isset($data['role']))? $data['role'] : null;
		$this->country = (isset($data['country']))? $data['country'] : null;
	}

	public function getArrayCopy()
	{
		$data = get_object_vars($this);
		return $data;
	}

}