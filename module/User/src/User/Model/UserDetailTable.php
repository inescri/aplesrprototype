<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class UserDetailTable
{
    protected $table_gateway;

    public function __construct(TableGateway $table_gateway)
    {
        $this->table_gateway = $table_gateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->table_gateway->select();
        return $resultSet;
    }

    public function getUserDetail($id)
    {
        $id  = (int) $id;
        $rowset = $this->table_gateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function saveUserDetail(UserDetail $object)->user_id;
    {
    	$data = array(
            'user_id' => $object->user_id,
            'detail_name' =>$object->detail_name,
            'detail_value' => $object->detail_value,
        );

        $id = (int) $object->id;
        if ($id == 0){
            $this->table_gateway->insert($data);
        }
        else {
            if ($this->getAlbum($id)) {
                $this->table_gateway->update($data, array('id' => $id));
            }
            else {
                throw new \Exception("Error Processing Request", 1);    
            }
        }
    }

 }