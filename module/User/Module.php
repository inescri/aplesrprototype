<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/User for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace User;

use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use User\Model\User;
use User\Model\UserTable;


class Module implements AutoloaderProviderInterface
{
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
		    // if we're in a namespace deeper than one level we need to fix the \ in the path
                    __NAMESPACE__ => __DIR__ . '/src/' . str_replace('\\', '/' , __NAMESPACE__),
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'UserTable' => function($sm) {
                    $table_gateway = $sm->get('UserTableGateway');
                    $table = new \User\Model\UserTable($table_gateway);
                    return $table;
                },
                'UserTableGateway' => function($sm) {
                    $db_adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultset_prototype = new ResultSet();
                    $resultset_prototype->setArrayObjectPrototype(new \User\Model\User());
                    return new TableGateway('user',$db_adapter,null,$resultset_prototype);
                },
                'UserDetailTable' => function($sm) {
                    $table_gateway = $sm->get('UserDetailTableGateway');
                    $table = new \User\Model\UserDetailTable($table_gateway);
                    return $table;
                },
                'UserDetailTableGateway' => function($sm) {
                    $db_adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultset_prototype = new ResultSet();
                    $resultset_prototype->setArrayObjectPrototype(new \User\Model\UserDetail());
                    return new TableGateway('user_detail',$db_adapter,null,$resultset_prototype);
                },

                'DummyUsers' => function($sm) {
                    $users = array();
                },
                //FORMS
                'User\Form\UserForm' => function($sm) {
                    return new \User\Form\UserForm();
                },
                //
                'UserFormFactory' => function ($sm) {
                    $form = $sm->get('User\Form\UserForm');
                    $view = new \Zend\View\Model\ViewModel(array('form' => $form));
                    $view->setTemplate('user/user/parts/create-user-form');
                    return $view;
                },

            )
        );
    }
    public function onBootstrap(MvcEvent $e)
    {
        // You may not need to do this if you're doing it elsewhere in your
        // application
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
    }
}
