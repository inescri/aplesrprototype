<?php 
namespace Mailer\Service;

use Zend\Mail;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class MailService implements ServiceLocatorAwareInterface
{
	protected $mailer;
	protected $sm;

	public function getMailerService()
	{
		if (!$this->mailer) {
			$this->mailer = $this->getServiceLocator()->get('MailerService');
		}

		return $this->mailer;
	}

	public function getEmailCopy()
	{
		$config = $this->getServiceLocator()->get('Config');
		$emails =  $config['email_config']['email_address'];
		if (is_array($emails)) {
			return $emails;
		}
		else {
			return explode(",", $emails);
		}
	}

	public function sendReviewInvitation()
	{
		$mailer = $this->getMailerService();
		$mail = new Mail\Message();
		$module_config = $this->getModuleConfig();
		
		$body = "Dear Manager, Please review you respondents: " . $module_config['review_respondent_link'];
		$mail->setBody($body);
		$mail->setSubject('Manager Invitation');
		$mail->setFrom($module_config['sender']);
		$mail->setTo($this->getEmailCopy() );
		$mailer->send($mail);
	}

	public function sendSurveyInvitation($contact = null, $subject = null)
	{
		if ($contact == null || $subject == null) return false;
		$mailer = $this->getMailerService();
		$module_config = $this->getModuleConfig();
		$mail = new Mail\Message();
		$body = "Dear ".$contact->fullname.",

We want to hear from you! As part of our ongoing efforts to understand our customers’ experiences, we are inviting you to take part in a short survey. 

To begin the survey, please click on the link below, which will only take you less than 1 minute to finish the online survey.  

".$module_config['survey_link']."

Survey ends October 31, 2014.

We sincerely hope to get your feedback shortly.

Regards,

APL

***This is a system generated email, please do not reply as it will not reach an APL mailbox.***
";

		$mail->setSubject('APL Sales Representative Survey Invitation');
		$mail->setBody($body);
		$mail->setFrom($module_config['sender']);
		$mail->setCc($this->getEmailCopy() );
		$mail->setTo($contact->email);
		$mailer->send($mail);
		
	}

	public function sendReminderInvitation($contact = null, $subject = null)
	{
		if ($contact == null || $subject == null) return false;
		$mailer = $this->getMailerService();
		$module_config = $this->getModuleConfig();
		$mail = new Mail\Message();
		$body = "Dear ".$contact->fullname.",
 
Please note that there is a survey for ".$subject->fullname." pending your completion.
 
Please click on the link below for the questionnaire.  

Survey ends October 31, 2014.

".$module_config['survey_link']."
 

Regards,
APL team in charge of ISQ system
 
***This is a system generated email, please do not reply as it will not reach an APL mailbox.***
		";
		$mail->setSubject('APL Sales Representative Survey Reminder');
		$mail->setBody($body);
		$mail->setFrom($module_config['sender']);
		$mail->setTo($this->getEmailCopy() );
		$mailer->send($mail);
	}

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
	{
		$this->sm = $serviceLocator;
	}

	public function getServiceLocator()
	{
		return $this->sm;
	}

	public function getModuleConfig()
	{
		$config = $this->getServiceLocator()->get('config');
		return $config['module_config'];
	}
}