<?php
namespace Reporting\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class SurveyListController extends AbstractActionController
{
    public function indexAction()
    {
        $questions = $this->getServiceLocator()->get('QuestionRepository');
        $filters_view = new ViewModel();
        $filters_view->setTemplate('reporting/dashboard/parts/filters');
        $view = new ViewModel(array(
            'questions' => $questions,
        ));
        $view->addChild($filters_view,'filters');
        return $view;
    }
}