<?php 

namespace Reporting\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\Plugin\Url;
use Zend\View\Model\ViewModel;
use Zend\Mail;
use PHPExcel;
use PHPExcel_IOFactory;

class LoaderController extends AbstractActionController
{
	protected $module_config;

	public function loadUserListAction()
	{
		$user_loader_form = $this->getServiceLocator()->get('UserLoaderForm');	
		$phpexcel_reader = PHPExcel_IOFactory::createReader('Excel2007');
		$error = null;
		$success = null;
		$data = array();
		if ($this->getRequest()->isPost()) {
			$user_loader_form->setData($this->getRequest()->getPost());
			$ext_valid = new \Zend\Validator\File\Extension(array('xls','xlsx'));
			$file = $this->params()->fromFiles('fileUpload');
			
			if (!$ext_valid->isValid($file)) {
				//$error = 'INVALID_FILE_EXT';
				$error = $ext_valid->getMessages();
			}
			else {
				$contact_table = $this->getServiceLocator()->get('ContactTable');
				$user_table = $this->getServiceLocator()->get('UserTable');
				$assignment_table = $this->getServiceLocator()->get('AssignmentTable');

				$phpexcel = $phpexcel_reader->load($file['tmp_name']);
				$worksheet = $phpexcel->getActiveSheet();
				$max_row =  $worksheet->getHighestRow();
				$max_col = $worksheet->getHighestColumn();
				$config = $this->getModuleConfig();
				$contact = new \Reporting\Model\Contact();
				$user = new \User\Model\User();
				//var_dump($config['required_fields']);
				for ($i = 2; $i <= $max_row; $i++) {
					$row = $worksheet->rangetoArray('A'.$i.':'.$max_col.$i)[0];	
					$data_contact = array(
						'fullname' => $row[5],
						'email' => $row[7],
					);

					$data_user = array(
						'firstname' => $row[0],
						'lastname' => $row[1],
						'role' => $row[3],
						'country' => $row[2],
						'email' => '',
						'username' => $row[0].'.'.$row[1],
					);

					if (!$contact_id = $contact_table->getContactbyEmail($data_contact['email'])) {
						$contact->exchangeArray($data_contact);
						$contact_table->saveContact($contact);
						echo '<br>save:' . $contact_id;
					}

					if (!$subject_id = $user_table->getUserbyName($data_user['firstname'],$data_user['lastname'])) {
						$user->exchangeArray($data_user);
						$user_table->saveUser($user);
						echo '<br>save:' . $data_contact['firstname'];
					}

					if (isset($contact_id->id) and isset($subject_id->id)) {
						$assignment = new \Reporting\Model\Assignment();
						$assignment->subject_id = $subject_id->id;
						$assignment->contact_id = $contact_id->id;
						if (!$assignment_table->exists($assignment->subject_id,$assignment->contact_id)) {
							$assignment_table->saveAssignment($assignment);
						}
					}


				}
				$success[] = 'List successfully inserted.';				
			}
		}
		$upload_form_view = new ViewModel(array(
			'userLoaderForm' => $user_loader_form,
			'error' => $error,
			'success' => $success,
		));
		$upload_form_view->setTemplate('reporting/loader/parts/upload-form');
		$view = new ViewModel();
		$view->addChild($upload_form_view,'uploadForm');
		return $view;
	}

	public function processFile($file)
	{

	}

	public function sendEmail()
	{
		$mailer = $this->getServiceLocator()->get('MailService');
		$mailer->sendReviewInvitation();
	}

	public function getModuleConfig() {
		if (!isset($this->module_config)) {
			$this->module_config = $this->getServiceLocator()->get('Config')['module_config'];
		}
		return $this->module_config;
	}
		
}
