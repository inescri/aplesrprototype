<?php

namespace Reporting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class AssignmentTable
{
	protected $table_gateway;

	public function __construct(TableGateway $table_gateway)
	{
		$this->table_gateway = $table_gateway;
	}

	public function fetchAll()
	{
		$result_set = $this->table_gateway->select();
		return $result_set;
	}

	public function getAssignment($id)
    {
        $id  = (int) $id;
        $rowset = $this->table_gateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function exists($subject_id, $contact_id) 
    {
        $rowset = $this->table_gateway->select(array('subject_id' => $subject_id, 'contact_id' => $contact_id));
        $row = $rowset->current();
        if (!$row) {
           return false;
        }
        return $row;
    }

    public function getAssignmentList()
    {

    	$result_set = $this->table_gateway->select(function (Select $select) {
    		
    		$select->join('contact','contact.id = assignment.contact_id',array('email','fullname'));
    		$select->join('user','user.id = assignment.subject_id',array('firstname','lastname'));
    		$select->columns(array());
    	} );

    	return $result_set;
    }

	public function saveAssignment(Assignment $object)
	{
		$data = array(
			'subject_id' => $object->subject_id,
			'contact_id' => $object->contact_id,
		);
		$id = (int) $object->id;
		if ($id == 0){
			$this->table_gateway->insert($data);
		}
		else {
			if ($this->getAlbum($id)) {
				$this->table_gateway->update($data, array('id' => $id));
			}
			else {
				throw new \Exception("Error Processing Request", 1);	
			}
		}
	}
}
