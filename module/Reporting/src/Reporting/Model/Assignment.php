<?php
namespace Reporting\Model;

class Assignment
{
	public $id;
	public $contact_id;
	public $subject_id;

	public function exchangeArray($data)
	{

		$this->id  = (isset($data['id'])) ? $data['id'] : null;
		$this->contact_id  = (isset($data['contact_id'])) ? $data['contact_id'] : null;
		$this->subject_id  = (isset($data['subject_id'])) ? $data['subject_id'] : null;
	}
}