<?php

namespace Reporting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;

class ContactTable
{
	protected $table_gateway;

	public function __construct(TableGateway $table_gateway)
	{
		$this->table_gateway = $table_gateway;
	}

	public function fetchAll()
	{
		$result_set = $this->table_gateway->select();
		return $result_set;
	}

	public function getContact($id)
    {
        $id  = (int) $id;
        $rowset = $this->table_gateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function getContactbyEmail($email) 
    {
        $rowset = $this->table_gateway->select(array('email' => $email));
        $row = $rowset->current();
        if (!$row) {
           return false;
        }
        return $row;
    }

    public function getSubject($id)
    {
    	$result_set = $this->table_gateway->select(function (Select $select) use ($id) {
    		$select->where(array('contact.id' => $id));
    		$select->join('assignment','contact.id = assignment.contact_id');
    	} );

    	return $result_set;
    }


	public function saveContact(Contact $contact)
	{
		$data = array(
			'fullname' => $contact->fullname,
			'email' => $contact->email,
		);
		$id = (int) $contact->id;
		if ($id == 0){
			$this->table_gateway->insert($data);
		}
		else {
			if ($this->getAlbum($id)) {
				$this->table_gateway->update($data, array('id' => $id));
			}
			else {
				throw new \Exception("Error Processing Request", 1);	
			}
		}
	}
}
