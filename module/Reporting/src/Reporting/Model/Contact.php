<?php
namespace Reporting\Model;

class Contact
{
	public $id;
	public $fullname;
	public $email;

	public function exchangeArray($data)
	{
		/*$vars = get_object_vars($this);
		foreach ($vars as $var) {
			$this->$var  = (!empty($data[$var])) ? $data[$var] : null;
		}*/
		$this->id  = (isset($data['id'])) ? $data['id'] : null;
		$this->fullname  = (isset($data['fullname'])) ? $data['fullname'] : null;
		$this->email  = (isset($data['email'])) ? $data['email'] : null;
	}
}